const localStore = {
    //本地数据存储部分
    setItem(key, value) {
      store.set(key, typeof value === 'object' ? JSON.stringify(value) : value)
    },
    getItem(key) {
      return store.get(key)
    },
    removeItem(key) {
      store.remove(key)
    },
    clear() {
      store.clearAll()
    }
}
export default localStore