export const apiStateList = [{
        "code": 0,
        "cn": "0：操作成功",
        "en": "0: operation successful",
        "tr": "0: işlem başarılı",
        "ar": "0: operation successful",
        "fa": "0: operation successful",
    },
    {
        "code": 1000,
        "cn": "1000：提交参数错误",
        "en": "1000: submission parameter error",
        "tr": "1000: gönderim parametresi hatası",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1001,
        "cn": "1001：推荐码无效",
        "en": "1001: the recommended code is invalid",
        "tr": "1001: önerilen kod geçersiz",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1002,
        "cn": "1002：数据库执行失败",
        "en": "1002: database execution failed",
        "tr": "1002: veritabanı çalıştırılamadı",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1003,
        "cn": "1003: failed to call the core RPC service",
        "en": "Success",
        "tr": "Başarılı",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1004,
        "cn": "1004：该邮箱已经注册",
        "en": "1004: this mailbox has been registered",
        "tr": "1004: bu posta kutusu kaydedildi",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1005,
        "cn": "1005：用户已经销户",
        "en": "1005: the user has logged out",
        "tr": "1005: kullanıcı oturumu kapattı",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1006,
        "cn": "1006：用户不存在",
        "en": "1006: the user does not exist",
        "tr": "1006: kullanıcı mevcut değil",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1007,
        "cn": "1007：请勿重复提交",
        "en": "1007: please do not repeat submit",
        "tr": "1007: lütfen göndermeyi tekrar etmeyin",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1008,
        "cn": "1008：可用金额不足",
        "en": "1008: insufficient amount available",
        "tr": "1008: yetersiz miktar mevcut",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1009,
        "cn": "1009：邮箱验证码错误",
        "en": "1009: email verification code error",
        "tr": "1009: e-posta doğrulama kodu hatası",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1010,
        "cn": "1010：邮箱发送失败",
        "en": "1010：email delivery failed",
        "tr": "1010 ： e-posta teslimi başarısız oldu",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1011,
        "cn": "1011：登录失效",
        "en": "1011: login lose efficacy",
        "tr": "1011: giriş etkinliğini kaybet",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1012,
        "cn": "1012：参数长度错误",
        "en": "1012: parameter length error",
        "tr": "1012: parametre uzunluğu hatası",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1013,
        "cn": "1013：用户等级错误",
        "en": "1013: user level error",
        "tr": "1013: kullanıcı düzeyinde hata",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1021,
        "cn": "1021：未添加银行卡",
        "en": "1021：No bank card was added",
        "tr": "1021 ： Banka kartı eklenmedi",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1022,
        "cn": "1022：未添加钱包地址",
        "en": "1022：No wallet address added",
        "tr": "1022 ： M-cüzdan adresi eklenmedi",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1040,
        "cn": "1040：手机号错误",
        "en": "1040：phone number is wrong",
        "tr": "1040 ： telefon numarası yanlış",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1031,
        "cn": "1031：用户当前有交易",
        "en": "1031：The user currently has transactions",
        "tr": "1031 ： Kullanıcının şu anda işlemleri var",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1051,
        "cn": "1051：调用MT5接口出错",
        "en": "1051：MT5 api error",
        "tr": "1051 ： MT5 api hatası",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1100,
        "cn": "1100：可用金额不足",
        "en": "1100：insufficient amount available",
        "tr": "1100 ： yetersiz miktar mevcut",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1061,
        "cn": "1061：时间错误",
        "en": "1061：time error",
        "tr": "1061 ： zaman hatası",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1101,
        "cn": "1101：当前存在理财信息不能申请",
        "en": "1101：Current financial information cannot be applied",
        "tr": "1101 ： Mevcut finansal bilgiler uygulanamaz",
        "ar": "",
        "fa": ""
    },
    {
        "code": 1102,
        "cn": "1102：理财券不存在",
        "en": "1102：Wealth coupons not exist",
        "tr": "1102 ： Servet kuponu yok",
        "ar": "",
        "fa": ""
    }
]