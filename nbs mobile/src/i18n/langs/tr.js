module.exports = {
    login: {
        'email': 'Email gereklidir'
    },
    register: {
        'email': 'Email gereklidir',
        'pwd': 'lütfen bir şifre girin',
        'pwdLengthTip': 'Lütfen 6-16 haneli şifre girin',
        'recommendCode': 'Lütfen hakem kodunu girin',
        'phone': 'Lütfen telefonu giriniz',
        'name': 'Lütfen adı girin',
        'age': 'Lütfen yaşı girin',
        'period': 'Lütfen yatırım dönemini girin',
        'reasons': 'Lütfen başvuru nedenini girin',
        'address': 'Lütfen adresi giriniz'
    },
    verify: {
        'surname': 'Soyadı boş olamaz',
        'name': 'İlk isim boş olamaz',
        'surnameEn': 'İngilizce soyadı boş olamaz',
        'nameEn': 'İngilizce Adı boş olamaz',
        'birth': 'Doğum tarihi boş olamaz',
        'sex': 'Cinsiyet boş olamaz',
        'credentialsType': 'Sertifika türü boş olamaz',
        'credentialsNum': 'Düzenleme tarihi boş olamaz',
        'credentialsDate': 'Düzenlenme tarihi boş olamaz',
        'credentialsDateLimit': 'Son kullanma tarihi boş olamaz',
        'zip': 'Posta kodu boş olamaz',
        'address': 'Adres boş olamaz',
        'city': 'Şehir boş olamaz',
        'province': 'Eyalet veya il boş olamaz'
    },
    publish: {
        'name': 'Ürün adı boş olamaz',
        'startTime': 'Başlangıç tarihi boş olamaz',
        'profitIncome': 'Yıllık gelir boş olamaz',
        'profitVal': 'Kar paylaşım oranı boş bırakılamaz',
        'cost': 'Promosyon komisyonu boş olamaz',
        'minQuota': 'Minimum bağış toplama miktarı boş olamaz',
        'maxQuota': 'Maksimum artırılan miktar boş olamaz',
    },
    changePwd: {
        'pwd': 'Lütfen eski şifreyi girin',
        'newPwd': 'Yeni şifre boş olamaz',
    },
    addAddress: {
        'address': 'M-cüzdan adresi boş olamaz'
    },
    addBank: {
        'name': 'İsim boş olamaz',
        'bankName': 'Banka adı boş olamaz',
        'openBank': 'SWIFT BIC veya ABA boş olamaz',
        'bankAccount': 'Banka hesabı boş olamaz',
    },
    operator: {
        'nickName': 'Takma isim boş olamaz'
    },
    settingSuperior: {
        'number': 'Üst numara boş bırakılamaz'
    },
    entrust: {
        'number': 'Tüccar kimliği boş olamaz',
        'profit': 'Kar payı boş bırakılamaz',
        'risk': 'Tasfiye oranı boş olamaz'
    }

}