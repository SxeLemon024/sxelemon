// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import './assets/rem'
import './assets/store'
// import './assets/charting_library.min';

import Vue from 'vue'
import App from './App'
import router from './router'



// import ElementUI from 'element-ui';

// import {Upload,Message,Loading,Dialog,DatePicker,MessageBox,Checkbox,CheckboxGroup,Form,FormItem,Input} from 'element-ui';
// import {Upload} from 'element-ui';

// import 'element-ui/lib/theme-chalk/index.css'


import axios from './assets/axios'

import filters from './utils/filter'

import i18n from './i18n/i18n'

import util from './utils/util'
import localStore from './utils/localStore'
import clipboard from 'clipboard'
import api from './assets/api'
import VueMeta from 'vue-meta'

import VueApexCharts  from 'vue-apexcharts';
Vue.use(VueApexCharts );
Vue.component('apexchart', VueApexCharts);
// Vue.use(Laue);

Vue.config.productionTip = false

// Vue.use(Upload);
// Vue.use(ElementUI);
Vue.use(VueMeta);

export const changLang = new Vue(); 


Vue.prototype.$axios = axios;

Vue.prototype.util = util;
Vue.prototype.localStore = localStore;
Vue.prototype.clipboard = clipboard;
Vue.prototype.$api = api;





/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  filters,
  i18n,
  components: { App },
  template: '<App/>'
})
