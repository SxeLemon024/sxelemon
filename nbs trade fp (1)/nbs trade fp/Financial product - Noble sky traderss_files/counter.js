(function($) {
$.fn.countTo = function(options){
    options = $.extend({}, $.fn.countTo.defaults, options || {});

    var loops = Math.ceil(options.speed / options.refreshInterval),
        increment = (options.to - options.from) / loops;

    return $(this).each(function(){
        var _this = this,
            loopCount = 0,
            value = options.from,
            interval = setInterval(updateTimer, options.refreshInterval);

        function updateTimer(){
            value += increment;
            loopCount++;
            $(_this).html(value.toFixed(options.decimals));

            if (typeof(options.onUpdate) == 'function'){
                options.onUpdate.call(_this, value);
            }

            if (loopCount >= loops){
                clearInterval(interval);
                value = options.to;

                if (typeof(options.onComplete) == 'function'){
                    options.onComplete.call(_this, value);
                }
            }
        }
    });
};

$.fn.countTo.defaults = {
    from: 0,
    to: 100,
    speed: 1000,
    refreshInterval: 100,
    decimals: 0,
    onUpdate: null,
    onComplete: null,
};
})(jQuery);

/** Target values **/
var tweets = 500000,
    followers = 150,
    following = 2;
	var fp1 = 500000,
    fpip = 50000,
    fpar = 60000;

/** Elements holding the values **/
var val_tweets = $('.clients'),
    val_followers = $('.experts'),
    val_following = $('.products');
	var val_maxraised1 = $('.fpmra1'),
    val_iprincipal1 = $('.fpip1'),
    val_amountRaised1 = $('.fpar1');
	var val_maxraised2 = $('.fpmra2'),
    val_iprincipal2 = $('.fpip2'),
    val_amountRaised2 = $('.fpar2');

/** Text description elements **/
var title_tweets = $('.title-tweets'),
    title_followers = $('.title-followers'),
    title_following = $('.title-following');
	var title_maxraised1 = $('.title-fpmra1'),
    title_iprincipal1 = $('.title-fpip1'),
    title_amountRaised1 = $('.title-fpar1');
	var title_maxraised2 = $('.ftitle-pmra2'),
    title_iprincipal2 = $('.title-fpip2'),
    title_amountRaised2 = $('.title-fpar2');

// setTimeout(function(){
    // animateCount();
// },1500);
//so that fufnction can be run when user scrolls there
var element = $('#datacounters');
var element2 = $('#datacounters2');
var element3 = $('#toprow');


var counter1=0;
var counter2=0;
var counter3=0;
function AnimateCountFunction() {
     animateCount();
}
function AnimateCountFunction2() {
     animateCount2();
}
function AnimateCountFunction3() {
     animateCard();
}

$(window).scroll(function(){
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();
	if($(element).offset()){
			var elemTop = $(element).offset().top;
			var elemBottom = elemTop + $(element).height();
    }
    if ($(element).offset()){
    var elemTop = $(element).offset().top;
    var elemBottom = elemTop + $(element).height();
    }
    if($(element3).offset()){
    var elemTop3 = $(element3).offset().top;
    var elemBottom3 = elemTop3 + $(element3).height();
}
    if ($(element).offset() && (elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
		if(counter1<1){
			
        AnimateCountFunction();
		counter1++;
		}
    }
    if($(element2).offset()){
	  var elemTop2 = $(element2).offset().top;
    var elemBottom2 = elemTop + $(element2).height();
}
    if ($(element2).offset() && (elemBottom2 <= docViewBottom) && (elemTop2 >= docViewTop)) {
		if(counter2<1){
			
        AnimateCountFunction2();
		counter2++;
		}
    }
	
	  if ($(element3).offset() && (elemBottom3 <= docViewBottom) && (elemTop3 >= docViewTop)) {
		if(counter3<1){
			
        AnimateCountFunction3();
		counter3++;
		}
    }
});
function animateCard(){
	
	$('#toprow').addClass('animateCardTop');
	$('#bottomrow').addClass('animateCardBottom');
}
function animateCount(){
	
    $('.restart-count').css('visibility','hidden');

    /** Reset colors **/
    title_tweets.css('color','#81878b');
    title_followers.css('color','#81878b');
    title_following.css('color','#81878b');
    
    /** Reset the values **/
    val_tweets.html("0");
    val_followers.html("0");
    val_following.html("0");

    val_tweets.countTo({
        from: 0,
        to: tweets,
        speed: 3000,
        refreshInterval:20,
        onComplete:function(){
            title_tweets.css('color','#669900');
			$('.clients').text("+ "+ tweets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            val_followers.countTo({
                from: 0,
                to: followers,
                speed: 3000,
                refreshInterval:20,
                onComplete:function(){
                    title_followers.css('color','#FF8800');
					$('.experts').text("+"+ followers)
                    val_following.countTo({
                        from: 0,
                        to: following,
                        speed: 1000,
                        refreshInterval:20,
                        onComplete:function(){
                            title_following.css('color','#CC0000');
                            $('.restart-count').css('visibility','visible');
                        }
                    });
                }
            });
        }
    });
}

/** Target values **/
var tweets = 500000,
    followers = 150,
    following = 2;
	var fp1 = 500000,
    fpip = 50000,
    fpar = 60000;

/** Elements holding the values **/
var val_tweets = $('.clients'),
    val_followers = $('.experts'),
    val_following = $('.products');
	var val_maxraised1 = $('.fpmra1'),
    val_iprincipal1 = $('.fpip1'),
    val_amountRaised1 = $('.fpar1');
	var val_maxraised2 = $('.fpmra2'),
    val_iprincipal2 = $('.fpip2'),
    val_amountRaised2 = $('.fpar2');

/** Text description elements **/
var title_tweets = $('.title-tweets'),
    title_followers = $('.title-followers'),
    title_following = $('.title-following');
	var title_maxraised1 = $('.title-fpmra1'),
    title_iprincipal1 = $('.title-fpip1'),
    title_amountRaised1 = $('.title-fpar1');
	var title_maxraised2 = $('.ftitle-pmra2'),
    title_iprincipal2 = $('.title-fpip2'),
    title_amountRaised2 = $('.title-fpar2');
function animateCount2(){
    $('.restart-count').css('visibility','hidden');

    /** Reset colors **/
    title_maxraised1.css('color','#81878b');
    title_iprincipal1.css('color','#81878b');
    title_amountRaised1.css('color','#81878b');
    
    /** Reset the values **/
    val_maxraised1.html("0");
    val_iprincipal1.html("0");
    val_amountRaised2.html("0");

    val_maxraised1.countTo({
        from: 0,
        to: fp1,
        speed: 3000,
        refreshInterval:20,
        onComplete:function(){
            title_tweets.css('color','#669900');
			$('.fpmra1').text( tweets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            val_iprincipal1.countTo({
                from: 0,
                to: fpip,
                speed: 3000,
                refreshInterval:20,
                onComplete:function(){
                    title_followers.css('color','#FF8800');
					$('.fpip1').text("+"+ fpip)
                    val_amountRaised2.countTo({
                        from: 0,
                        to: following,
                        speed: 1000,
                        refreshInterval:20,
                        onComplete:function(){
                            title_amountRaised1.css('color','#CC0000');
                            $('.restart-count').css('visibility','visible');
                        }
                    });
                }
            });
        }
    });
}

$('.restart-count').click(function(){
    animateCount();
});

$(window).on("load resize ", function() {
  var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
  $('.tbl-header').css({'padding-right':scrollWidth});
}).resize();

$(document).ready(function () {
  $('#tradingTable').DataTable({searching: false, lengthChange: false, info: false, pageLength :5});/*,bFilter:false*/
  $('#orderTable').DataTable({searching: false, lengthChange: false, info: false, pageLength :5,pagingType: "full_numbers"});/*,bFilter:false*/
  $('#rebateTable').DataTable({searching: false, lengthChange: false, info: false, pageLength :5,pagingType: "full_numbers"/*,bFilter:false*/
  });
    
});
