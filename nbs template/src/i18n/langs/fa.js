module.exports = {
    login: {
        'email': 'Email is required'
    },
    register: {
        'email': 'Email is required',
        'pwd': 'Please enter a password',
        'pwdLengthTip': 'Please enter 6-16 digit password',
        'recommendCode': 'Please enter the referee code',
        'phone': 'Please enter the phone',
        'name': 'Please enter the name',
        'age': 'Please enter the age',
        'period': 'Please enter investment period',
        'reasons': 'Please enter the application reason',
        'address': 'Please enter the address'
    },
    verify: {
        'surname': 'The surname cannot be empty',
        'name': 'The First name cannot be empty',
        'surnameEn': 'The English surname cannot be empty',
        'nameEn': 'English First name cannot be empty',
        'birth': 'Date of birth cannot be empty',
        'sex': 'The sex cannot be empty',
        'credentialsType': 'Type of ceryificate cannot be empty',
        'credentialsNum': 'Issue date cannot be empty',
        'credentialsDate': 'The date of issue cannot be empty',
        'credentialsDateLimit': 'The date of expiry cannot be empty',
        'zip': 'The zip code cannot be empty',
        'address': 'The address cannot be empty',
        'city': 'The city cannot be empty',
        'province': 'The state or province cannot be empty'
    },
    publish: {
        'name': 'Product name cannot be empty',
        'startTime': 'Start date cannot be empty',
        'profitIncome': 'Annualized income cannot be empty',
        'profitVal': 'Profit sharing ratio cannot be empty',
        'cost': 'Promotion commission cannot be empty',
        'minQuota': 'The minimum fundraising amount cannot be empty',
        'maxQuota': 'The maximum raised amount cannot be empty',
    },
    changePwd: {
        'pwd': 'Please enter the old password',
        'newPwd': 'The new password cannot be empty',
    },
    addAddress: {
        'address': 'The wallet address cannot be empty'
    },
    addBank: {
        'name': 'Name cannot be empty',
        'bankName': 'Bank name annot be empty',
        'openBank': 'SWIFT BIC or ABA cannot be empty',
        'bankAccount': 'Bank account cannot be empty',
    },
    operator: {
        'nickName': 'Nick name cannot be empty'
    },
    settingSuperior: {
        'number': 'Superior number cannot be empty'
    },
    entrust: {
        'number': '操盘手编号不能为空',
        'profit': '盈利分成不能为空',
        'risk': '强平比例不能为空'
    }

}