module.exports = {
  login: {
    'email': '邮箱不能为空'
  },
  register:{
     'email': '邮箱不能为空',
     'pwd': '密码不能为空',
     'pwdLengthTip': '请设置长度为6-16位的密码',
     'recommendCode': '请输入推荐码',
     'phone':'请输入手机号',
     'name':'请输入姓名',
     'age':'请输入年龄',
     'period':'请输入投资年限',
     'reasons':'请输入申请原因',
     'address':'请输入地址'
  },
  verify:{
     'surname':'姓不能为空',
     'name':'名不能为空',
     'surnameEn':'英文姓不能为空',
     'nameEn':'英文名不能为空',
     'birth':'生日不能为空',
     'sex':'性别不能为空',
     'credentialsType':'请选择证件类型',
     'credentialsNum':'证件号不能为空',
     'credentialsDate':'签发日期不能为空',
     'credentialsDateLimit':'有效截止日期不能为空',
     'zip':'邮编不能为空',
     'address':'地址不能为空',
     'city':'城市不能为空',
     'province':'州或省份不能为空'

  },
  publish:{
     'name':'产品名称不能为空',
     'startTime':'开始日期不能为空',
     'profitIncome':'年化收益不能为空',
     'profitVal':'盈利分成比例不能为空',
     'cost':'推广佣金不能为空',
     'minQuota':'最低募集额度不能为空',
     'maxQuota':'最大募集额度不能为空',
  },

  changePwd:{
     'pwd': '请输入旧密码',
     'newPwd': '新密码不能为空',
  },
  addAddress:{
     'address': '钱包地址不能为空'
  },
  addBank:{
     'name': '姓名不能为空',
     'bankName': '银行不能为空',
     'openBank': '开户行名称不能为空',
     'bankAccount': '银行账号不能为空',
  },
  operator:{
     'nickName': '昵称不能为空'
  },
  settingSuperior:{
     'number': '上级编号不能为空'
  },
  entrust:{
     'number': '操盘手编号不能为空',
     'profit': '盈利分成不能为空',
     'risk': '强平比例不能为空'
  }


}
