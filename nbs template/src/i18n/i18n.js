// // import Vue from "vue";
// // import VueI18n from "vue-i18n";
// // import locale from 'element-ui/lib/locale'
// //
// // import languagePack from "./langs";
// // import localStore from '../utils/localStore';
//
//
// import Vue from 'vue'
// import VueI18n from 'vue-i18n'
// import locale from 'element-ui/lib/locale';
// import zh from './langs/zh'
// import en from './langs/en'
// import enLocale from 'element-ui/lib/locale/lang/en'
// import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
//
//
//
//
//
//
// // const.__lookupGetter__()
// const storeLanguage = localStore.getItem('curLanguage');
// const lan = !!storeLanguage?storeLanguage:2;
// Vue.use(VueI18n);
// const i18n = new VueI18n({
//   locale: lan==1?'zh':'en',
//   languagePack,
//   silentTranslationWarn:true
// });
// locale.i18n((key, value) => i18n.t(key, value))
//
// export default i18n;



import Vue from 'vue'
import Element from 'element-ui'
import VueI18n from 'vue-i18n'
import locale from 'element-ui/lib/locale';
import zh from './langs/zh'
import en from './langs/en'
import tr from './langs/tr'
import ar from './langs/ar'
import fa from './langs/fa'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import trLocale from 'element-ui/lib/locale/lang/tr-TR'
import arLocale from 'element-ui/lib/locale/lang/ar'
import faLocale from 'element-ui/lib/locale/lang/fa'
import localStore from '../utils/localStore';

Vue.use(VueI18n)

const messages = {
    en: Object.assign(en, enLocale),
    zh: Object.assign(zh, zhLocale),
    tr: Object.assign(tr, trLocale),
    ar: Object.assign(ar, arLocale),
    fa: Object.assign(fa, faLocale)
}


// console.log(messages.zh)


const storeLanguage = localStore.getItem('curLanguage');
const lanNo = !!storeLanguage ? storeLanguage : 2;
const lan = lanNo == 1 ? 'zh' : lanNo == 5 ? 'fa' : lanNo == 4 ? 'ar' : (lanNo == 3 ? 'tr' : 'en')



const i18n = new VueI18n({
        locale: lan,
        messages
    })
    //


// locale.i18n((key, value) => i18n.t(key, value)) //为了实现element插件的多语言切换

Vue.use(Element, {
    i18n: (key, value) => i18n.t(key, value)
})


export default i18n