import localStore from './localStore'
//公共工具
const util = {
  //判断是否是微信
  isWeChat() {
    let ua = window.navigator.userAgent.toLowerCase();
    return (ua.match(/MicroMessenger/i) == 'micromessenger')
  },
  //判断是否登录
  isLogin() {
    let token = localStore.getItem('userLoginInfo');
    return !!token
  },
    //判断是否是数字
  isIntNumber(num) {

      let moreg = /^[0-9]*[1-9][0-9]*$/
      return moreg.test(num)
  },
  isNumber(num) {

      let moreg = /^[+-]?\d*\.?\d*$/
      return moreg.test(num)
  },

  clearSpace(val){

    let reg = / /g;
    return val.replace(reg, "");
  },

  //是否是数字和字母的组合
  isLettersAndNumbers(val){
     let moreg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]*$/;
     return moreg.test(val)
  },
  //checks for whitespaces,returns,tabs and character escapes 
  isValidPassword(val) {
    let moreg = /[\w\[\]`!@#$%\^&*()={}:;<>+'-]*$/;
    return moreg.test(val);
  },

  //获取用户信息
  getUserInfo(key) {
    if (!this.isLogin()) return
    let u = localStore.getItem('user');
    let user = JSON.parse(u);
    if (key != undefined && key != '') {
      return user[key]
    } else {
      return user
    }
  },
  //判断是否是手机号码
  isMobile(mobile) {
    if (mobile) {
      let moreg = /^1\d{10}$/
      return moreg.test(mobile.trim())
    } else {
      return false
    }
  },
  //判断密码是否符合规则
  isPwd(pwd) {
    if (pwd) {
      pwd = pwd.trim()
      let re = /^[a-zA-Z0-9~@#$%&_]{6,16}$/
      if (!re.test(pwd)) return false;
      return !(pwd.length < 6 || pwd.length > 16)
    } else {
      return false;
    }
  },
  //创建表单
  MakeForm(json, url, el) {
    // 创建一个 form
    let form1 = document.createElement("form");
    form1.id = "tempfrm";
    form1.name = "tempfrm";
    // 添加到 body 中
    // document.body.appendChild(form1);
    el.appendChild(form1);
    for (let key in json) {
      // 创建一个输入
      let input = document.createElement("input");
      // 设置相应参数
      input.type = "hidden";
      input.name = key;
      input.value = json[key];
      // 将该输入框插入到 form 中
      form1.appendChild(input);
    }
    // form 的提交方式
    form1.method = "POST";
    // form 提交路径
    form1.action = url;
    // 对该 form 执行提交
    console.log(form1);
    // return;
    form1.submit();
    el.removeChild(form1);
  },
  //判断是否是身份号码
  isIDCardNum(num) {
    num = num.toUpperCase();
    //身份证号码为18位，18位前17位为数字，最后一位是校验位，可能为数字或字符X。
    if (!(/(^\d{17}([0-9]|X)$)/.test(num))) {
      return false;
    }
    //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
    //下面分别分析出生日期和校验位
    let re, nTemp = 0, len = num.length, arrInt = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2], arrCh = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
    if (len === 18) {
      re = new RegExp(/^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/);
      let arrSplit = num.match(re);
      //检查生日日期是否正确
      let dtmBirth = new Date(arrSplit[2] + "/" + arrSplit[3] + "/" + arrSplit[4]);
      let bGoodDay;
      bGoodDay = (dtmBirth.getFullYear() == Number(arrSplit[2])) && ((dtmBirth.getMonth() + 1) == Number(arrSplit[3])) && (dtmBirth.getDate() == Number(arrSplit[4]));
      if (!bGoodDay) {
        return false;
      } else {
        //检验18位身份证的校验码是否正确。
        //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
        let valnum;
        for (let i = 0; i < 17; i++) {
          nTemp += num.substr(i, 1) * arrInt[i];
        }
        valnum = arrCh[nTemp % 11];
        return arrCh[nTemp % 11] == num.substr(17, 1)
      }
    }
    return false;
  },
  isNull(value) {
    return (!value || value == '' || value == 'undefined' || value == 'null');
  },
 isIOS(){
			var u = navigator.userAgent;
			var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			console.log(isIOS);
		  return isIOS;
  },
  isIOS(){
 			var u = navigator.userAgent;
 			var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
 			console.log(isIOS);
 		  return isIOS;
  },

isEmail(email){
   let reg = new RegExp("^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$");
   return reg.test(email)
},

  //
  getYmd(n) {
    let date = new Date(n);
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    return (Y + M + D);
  },
  isNumInt(num) {
  	 if(num){
  		 let r = /^\+?[1-9][0-9]*$/;
  		 return r.test(num);
  	 }else{
  		 return false
  	 }
  }

}
export default util
