import Vue from 'vue'

//手机号码中间位变*
export function muskMobile(mobile) {
  if(!!mobile) {
    mobile += '';
    return mobile.substr(0, 3) + '****' + mobile.substr(-4);
  }
  return '';
}



 


//逗号分隔多位数的数字
export function toMoney(n) {
  let num = (n || 0).toString(),
    result = '';
  let arr = num.split('.');
  num = arr[0];
  while(num.length > 3) {
    result = ',' + num.slice(-3) + result;
    num = num.slice(0, num.length - 3);
  }
  if(num) {
    result = num + result;
  }
  if(arr.length > 1) {
    result += '.' + arr[1].substr(0,2);
  } else {
    result += '.00';
  }
  return result;
}

//银行卡*遮罩
export function maskBank(code){
  if(!!code){
    return '****  ****  ****' + code.substr(-4)
  }
  return ''
}

//取年月日
export function parseDate(date){
  if(!!date){
    return date.substr(0,10)
  }
}


const filters = {
  muskMobile,
  toMoney,
  parseDate,
  maskBank
}

export default filters

Object.keys(filters).forEach(key => {
  Vue.filter(key,filters[key])
})