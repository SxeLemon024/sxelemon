// const host = 'http://api.ukgr.hk'
// const host = 'http://192.168.0.33:50127'

const host = 'http://api.tfbmarking.com' //tfb

const host3 = 'https://crmapi.nbstrader.com' //tfb

// const host3 ='http://15.185.38.215:8899/'

// const host2 = 'http://coreapi.ukgr.hk'
const host2 = 'http://coreapi.tfbmarking.com' //tfb



// const host2 = 'http://54.255.151.207:8080'
// const host2 = 'http://192.168.0.27:8080'


//
// const nc_appkey = 'FFFF00000000016B11C5'  //应用标识,不可更改
const config = {
  // base: process.env.NODE_ENV === 'testing' ? '/task/' : '/',
  host,
//   nc_appkey,
//   nc_token: [nc_appkey, (new Date()).getTime(), Math.random()].join(':'),
  // imgUploadURL:'http://img.ukgr.hk/UploadFile.ashx',

  imgUploadURL:'https://img.nbstrader.com/UploadFile.ashx', //tfb


  //现货
  websocketPriceURL:'ws://hq.tfbmarking.com:8007',

 //合约
  websocketPriceURLContract:'ws://hq.tfbmarking.com:8008',//tfb


// //期货
  websocketPriceURLFutures:'ws://hq.tfbmarking.com:8009',

  // websocketPriceURLFutures:'ws://139.198.14.42:8009',//国内



  // websocketPriceURL:'ws://47.56.183.231:8007',
  url:{
      getCountry: `${host3}/api/Country`,//获取国籍
      // getCountry: `${host}/UserApi.ashx?action=getcountry`,//获取国籍
      sendCode: `${host}/UserApi.ashx?action=getmailcode`,//注册发送邮箱验证码
      registerType: `${host}/UserApi.ashx?action=getregister`,//注册方式
      register: `${host3}/api/Register`,//注册
      simulation: `${host3}/api/Simulation`,//申请模拟用户
      imitateregister: `${host}/UserApi.ashx?action=imitateregister`,//模拟注册

      getsystem: `${host}/UserApi.ashx?action=getsystem`,//查询系统交易模式
      getcurrency: `${host}/UserApi.ashx?action=getcurrency`,//查询可入金币种
      getwithdrawcurrency:`${host}/UserApi.ashx?action=getwithdrawcurrency`,//查询可出金币种
      getcurrencysystem:`${host}/UserApi.ashx?action=getcurrencysystem`,//查询系统币种设置

      getBankrollRecord: `${host}/user/userinfo.ashx?action=getcassetsflow`,//查询资金流水
      getLockInfoList: `${host}/user/userinfo.ashx?action=getsystemlocklist`,//查询全部币种锁仓信息
      getLockInfo: `${host}/user/userinfo.ashx?action=getsystemlock`,//查询锁仓信息

      userInfo: `${host3}/api/User`,//用户信息
      // getUserCountry: `${host}/api/Country`,//获取用户国籍
      userVerify1: `${host3}/api/UserInfo1`,//用户实名1
      // addUserVerify1: `${host3}/api/UserInfo1`,//新增用户实名1
      editUserVerify1: `${host}/user/userinfo.ashx?action=edituserinfo_u1`,//编辑用户实名1

      // getUserVerify2: `${host}/user/userinfo.ashx?action=getuserinfo_u2`,//获取用户实名2
      userVerify2: `${host3}/api/UserInfo2`,//用户实名2
      // editUserVerify2: `${host}/user/userinfo.ashx?action=edituserinfo_u2`,//编辑用户实名2

      userVerify3: `${host3}/api/UserInfo3`,//获取用户实名3
      // addUserVerify3: `${host}/user/userinfo.ashx?action=adduserinfo_u3`,//新增用户实名3
      // editUserVerify3: `${host}/user/userinfo.ashx?action=edituserinfo_u3`,//编辑用户实名3

      addcassets: `${host}/user/userinfo.ashx?action=addcassets`,//开通币币交易资产
      addaassets: `${host}/user/userinfo.ashx?action=addaassets`,//开通合约交易资产
      addfassets: `${host}/user/userinfo.ashx?action=addfassets`,//开通期货交易资产

      openexchange: `${host}/user/userinfo.ashx?action=openexchange`,//开通币币交易
      opencontract: `${host}/user/userinfo.ashx?action=opencontract`,//开通合约交易
      openfutures: `${host}/user/userinfo.ashx?action=openfutures`,//开通期货交易

      changePwd: `${host3}/api/Password`,//用户修改密码
      addtransfer: `${host3}/api/Transfer`,//资金划转
      addexchange: `${host}/user/userinfo.ashx?action=addexchange`,//币种兑换
      getatradesprice: `${host}/user/userinfo.ashx?action=getatradesprice`,//币种兑换


      ///法币//

      userFund:`${host3}/api/Fund`,//用户资金统一接口

      getuserfiatorder: `${host3}/api/Fiatorder`,//获取法币订单
      adddeposit: `${host3}/api/Deposit`,//增加法币购买订单

      getOrderBankCard: `${host3}/api/UserBank`,//获得订单收款方银行卡
      getsystemwallet: `${host}/user/userinfo.ashx?action=getsystemwallet`,//获得订单收款方比特币钱包

      getapipaymentlist: `${host3}/api/Payment`,//获取支付通道
      getpayfiatorder: `${host3}/api/Fiatorder`,//获取法币订单详细
      getinsystembank: `${host3}/api/UserBank`,//获得当前系统使用收款银行卡

      uploadOrderImg: `${host3}/api/Payfiatorder`,//购买订单上传凭证
      getuserbank: `${host3}/api/UserBank`,//获取用户提现银行卡
      getuserwallet: `${host3}/api/Virtual`,//获取用户虚拟币钱包

      adduserbank: `${host3}/api/UserBank`,//添加用户提现银行卡
      adduserwallet: `${host3}/api/Virtual`,//添加用户钱包地址

      addwithdraw: `${host3}/api/Withdraw`,//增加法币出售订单
      closeOrder: `${host3}/api/Fiatorder`,//关闭订单

      gettakegoods: `${host}/user/userinfo.ashx?action=gettakegoods`,//查询可提货商品
      addtakedelivery: `${host}/user/userinfo.ashx?action=addtakedelivery`,//新增提货商品
      gethtakedelivery: `${host}/user/userinfo.ashx?action=gethtakedelivery`,//提货信息列表
      gethtakedeliverybyid:`${host}/user/userinfo.ashx?action=gethtakedeliverybyid`,//提货信息列表
      applyfund:`${host}/user/userinfo.ashx?action=applyfund`,//模拟用户申请资金


      // getLoginLog: `${host}/user/userinfo.ashx?action=getloginlog`,//用户登录日志
      getDolog: `${host}/user/userinfo.ashx?action=getdolog`,//用户操作日志

      queryAsset: `${host2}/query_asset/`,//用户资产查询
      login: `${host3}/api/Login`,//登录
      order: `${host2}/order/`,//订单
      rateInfo: `${host3}/api/ExchangeRate`,//汇率
      orderList: `${host2}/query_order/`,//委托列表
      delOrder: `${host2}/del_order/`,//撤消委托
      // orderHistory: `${host2}/query_order_history/`,//历史委托

      // orderHistory: `${host2}/query_order_history_day/`,//历史委托
      orderHistory: `${host}/user/userinfo.ashx?action=getuserchentrust`,//成交历史

      // tradHistory: `${host2}/query_deal_history/`,//成交历史
      // tradHistory: `${host2}/query_deal_history_day/`,//成交历史
      tradHistory: `${host}/user/userinfo.ashx?action=getuserchistory`,//成交历史

      symbolList: `${host2}/all_goods_info/`,//所有交易对列表
      itemSymbol: `${host2}/good_info/`,//单个商品交易对
      asset: `${host2}/query_asset/`,
      assetLockInfo: `${host2}/query_lock_money/`, //锁仓明细
      applyLockPosition: `${host2}/apply_lock_position/`,//申请锁仓

      currency_info: `${host3}/api/Currency`,//查询币种


      //做市商
      make_market_all_goods_info: `${host2}/make_market_all_goods_info/`,//做市商商品列表
      make_market_goods_info: `${host2}/make_market_goods_info/`,//做市商单个商品
      make_market_order: `${host2}/make_market_order/`,//做市商下单
      make_market_query_asset: `${host2}/make_market_query_asset/`,//查询资产
      make_market_query_position: `${host2}/make_market_query_position/`,//持仓列表
      make_market_change_stop_loss: `${host2}/make_market_change_stop_loss/` ,//设置止损止盈
      make_market_query_order: `${host2}/make_market_query_order/`, //挂单列表
      make_market_cancel_order: `${host2}/make_market_cancel_order/`, //挂单撤单


      getuserahentrust: `${host}/actuals/history.ashx?action=getuserahentrust`,//下单历史
      getuserahistory: `${host}/actuals/history.ashx?action=getuserahistory`,//成交历史
      getauserpllist: `${host}/actuals/history.ashx?action=getauserpllist`,//修改止损止盈历史
      getaassetsflow: `${host}/actuals/history.ashx?action=getaassetsflow`,//查询资金流水


      //期货
      futures_trade_groups: `${host2}/futures_trade_groups/`,//期货商品列表
      futures_query_asset: `${host2}/futures_query_asset/`,//期货查询资产
      futures_query_trade_goods_by_group: `${host2}/futures_query_trade_goods_by_group/`,//根据分类id查询商品
      futures_query_default_goods:`${host2}/futures_query_default_goods/`,//未登录的用户默认的商品分组
      getfoptionalttrades: `${host}/user/userinfo.ashx?action=getfoptionalttrades`,//查询自选商品列表
      editoneftradesoptionalt:`${host}/user/userinfo.ashx?action=editoneftradesoptionalt`,
      futures_trade_goods_array:`${host2}/futures_trade_goods_array/`,//用户传入商品编码数组，服务器响应传入的商品列表信息给客户端
      futures_trade_goods:`${host2}/futures_trade_goods/`,//登录用户默认商品
      futures_query_settle_currency:`${host2}/futures_query_settle_currency/`,//获取用户风控结算币种





      getuserfhistory:`${host}/futures/history.ashx?action=getuserfhistory`, ///期货成交历史,
      getuserfhentrust:`${host}/futures/history.ashx?action=getuserfhentrust`, ///期货查询委托历史,

      getfassetsflow:`${host}/futures/history.ashx?action=getfassetsflow`, ///期货查询资金流水,
      getfuserpllist:`${host}/futures/history.ashx?action=getfuserpllist`, ///期货修改止损止盈历史,
      futures_query_position: `${host2}/futures_query_position/`,//期货持仓列表
      futures_query_order: `${host2}/futures_query_order/`,//期货挂单列表
      futures_order:`${host2}/futures_order/`,//期货下单
      futures_cancel_order:`${host2}/futures_cancel_order/`,//期货撤单
      futures_change_stop_loss:`${host2}/futures_change_stop_loss/`,//期货设置止损止盈


     //状态码
     apiResultState: `${host}/result.json`,




     //操盘手
     futures_proxy_query_position: `${host2}/futures_proxy_query_position/`,//操盘手查询持仓列表
     futures_proxy_query_order: `${host2}/futures_proxy_query_order/`,//操盘手查询挂单列表
     futures_proxy_get_all_childrens: `${host2}/futures_proxy_get_all_childrens/`,//操盘手查询下级
     futures_proxy_order: `${host2}/futures_proxy_order/`,//操盘手下单


     editusertype: `${host}/user/trader.ashx?action=editusertype`,//成为操盘手
     updatestrader: `${host}/user/trader.ashx?action=updatestrader`,//设置操盘手上级
     gettrader: `${host}/user/trader.ashx?action=gettrader`,//获取操盘手资料
     updatetrader: `${host}/user/trader.ashx?action=updatetrader`,//修改操盘手资料
     updateauthorized: `${host}/user/trader.ashx?action=updateauthorized`,//用户托管账户
     // getauthorized: `${host}/user/trader.ashx?action=getauthorized`,//获取当前托管信息
     gettraderhlist: `${host}/user/trader.ashx?action=gettraderhlist`,//操盘手查询下属成交历史
     gettraderpllist: `${host}/user/trader.ashx?action=gettraderpllist`,//操盘手查询下属止损止盈
     gettraderfhlist: `${host}/user/trader.ashx?action=gettraderfhlist`,//操盘手查询下属资金变动流水
     gettraderehlist: `${host}/user/trader.ashx?action=gettraderehlist`,//操盘手查询下属下单历史
     getauthorizedlist: `${host}/user/trader.ashx?action=getauthorizedlist`,//操盘手查询下属托管流水
     updatetraderstr: `${host}/user/trader.ashx?action=updatetraderstr`,//操盘手悠改模板
     closeauthorized: `${host}/user/trader.ashx?action=closeauthorized`,//操盘手结束托管



    //理财产品
    FinancialProducts: `${host3}/api/FinancialProducts`,//查询理财产品列表
    FinancialOrder: `${host3}/api/FinancialOrder`,//新增理财订单
    FinancialArticle: `${host3}/api/FinancialArticle`,//理财文章
    FinancialTradHistory: `${host3}/api/Deal`,//理财文章
    getCoupon: `${host3}/api/Coupon`,//领取理财券
    // FinancialProducts: `${host3}/api/FinancialProducts`,//增加理财商品
  },

  coins:[
    {value:1,name:'人民币',code:'CNY'},
    {value:2,name:'美元',code:'USD'}
//     {value:3,name:'比索',code:'PHP'},
//     {value:4,name:'港元',code:'HKD'},
//     {value:5,name:'日元',code:'JPY'},
//     {value:6,name:'韩元',code:'KRW'}
  ],

  defaultCoinType:1,
  defaultLanguage:2,
  ErrorHandleType: {
    NONE: 0,
    ALERT: 1,
    MESSAGE: 2,
    FOCUS: 3
  }
}
export default config