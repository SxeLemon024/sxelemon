//import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
//import router from 'src/router'
import localStore from '../utils/localStore';

axios.defaults.timeout = 1000000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'


// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    config.headers = config.headers || {}
//  const token = storage.get('x-token')
//  if (!!token) {
//    config.headers['x-token'] = token
//  }
    if (config.method === 'post') {

			 if(!!config.data.getAll){
				var data = config.data.getAll('file')[0];
									console.log(data);
									console.log('aab')
			 }else{
				 config.data = qs.stringify(config.data);
			 }


            // config.data = qs.stringify(config.data);



    }
    return config
  }, function (error) {
    // 对请求错误做些什么<img alt=''
    if(error.message.includes('timeout')){   // 判断请求异常信息中是否含有超时timeout字符串

          localStore.setItem('xTimeout',true)
          return Promise.reject(error);          // reject这个错误信息
        }

    return Promise.reject(error)
  });

//// 添加响应拦截器
//axios.interceptors.response.use(function (response) {
//  if (response.data && response.data.code === 401) {
//    storage.clearAll()
//    router.replace({
//      path: '/login',
//      query: { redirect: router.currentRoute.fullPath }
//    })
//    return Promise.reject(response.data)
//  }
//  return response
//}, function (error) {
//  return Promise.reject(error)
//});

export default axios

// Vue.use(axios)
//Vue.prototype.$axios = axios
