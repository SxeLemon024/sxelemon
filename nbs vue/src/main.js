// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import './assets/rem'
import './assets/store'
// import './assets/charting_library.min';

import Vue from 'vue'
import App from './App'
import router from './router'
import VueCurrencyFilter from 'vue-currency-filter'

import $ from 'jquery'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import 'bootstrap-vue/dist/bootstrap-vue.css'


//import * as mdb from 'mdb-ui-kit'; 

// import ElementUI from 'element-ui';

// import {Upload,Message,Loading,Dialog,DatePicker,MessageBox,Checkbox,CheckboxGroup,Form,FormItem,Input} from 'element-ui';
// import {Upload} from 'element-ui';

// import 'element-ui/lib/theme-chalk/index.css'


import axios from './assets/axios'

import filters from './utils/filter'

import i18n from './i18n/i18n'

import util from './utils/util'
import localStore from './utils/localStore'
import clipboard from 'clipboard'
import api from './assets/api'
import VueMeta from 'vue-meta'
import VueCarousel from 'vue-carousel';
//import mdb from '../node_modules/mdb-ui-kit/js/mdb.min.js';
import { Laue } from 'laue';
//import bxslider from './assets/bxslider.js'
//Vue.use(bxslider)

Vue.use(Laue);

Vue.use(VueCarousel);

Vue.config.productionTip = false
Vue.config.devtools = true
Vue.config.silent = true

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// Vue.use(Upload);
// Vue.use(ElementUI);
Vue.use(VueMeta);

Vue.use(VueCarousel);

Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 0,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true,
    avoidEmptyDecimals: undefined,
})

Vue.prototype.$axios = axios;

Vue.prototype.util = util;
Vue.prototype.localStore = localStore;
Vue.prototype.clipboard = clipboard;
Vue.prototype.$api = api;

export const changLang = new Vue();

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    filters,
    i18n,
    components: { App },
    template: '<App/>'
})
