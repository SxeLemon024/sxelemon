export const languageCode = {
    1: 'zh',
    2: 'en',
    3: 'tr',
    4: 'ar',
    5: 'ir'
}
