import Vue from 'vue'
import Router from 'vue-router'




const Home = () =>
    import ('@/views/home')
const Products = () =>
    import ('@/views/product-introduce')
const Publish = () =>
    import ('@/views/publish')
const FOrder = () =>
    import ('@/views/f-order')
const MyProducts = () =>
    import ('@/views/my-products')
const ArticleList = () =>
    import ('@/views/article-list')
const ArticleDetails = () =>
    import ('@/views/article-details')
const Contract = () =>
    import ('@/views/contract')
    //const ContractEN = () =>
    //    import ('@/views/contract-en')
const ActivityList = () =>
    import ('@/views/activity-list')
const Agent = () =>
    import ('@/views/agent')
const MT5 = () =>
    import ('@/views/mt5')

// const Mt5Mac = () => import('@/views/page/mt5-mac')
const Mt5Web = () =>
    import ('@/views/mt5-webtrader')
const Mt5Iphone = () =>
    import ('@/views/mt5-iphone')
const Mt5Android = () =>
    import ('@/views/mt5-android')

const ForexTrading = () =>
    import ('@/views/forex-trading')
const Commodities = () =>
    import ('@/views/commodities')
const EquityIndices = () =>
    import ('@/views/equity-indices')
const PreciousMetals = () =>
    import ('@/views/precious-metals')
const Energies = () =>
    import ('@/views/Energies')
    // const Shares = () => import('@/views/page/shares')
const ExecutionPolicy = () =>
    import ('@/views/execution-policy')
const Spreads = () =>
    import ('@/views/spreads')
const MarginAndLeverage = () =>
    import ('@/views/margin-and-leverage')
const OvernightPositions = () =>
    import ('@/views/overnight-positions')
const TradingHours = () =>
    import ('@/views/trading-hours')
const About = () =>
    import ('@/views/about')
const Regulation = () =>
    import ('@/views/regulation')
const LegalDocuments = () =>
    import ('@/views/legal-documents')
const FAQ = () =>
    import ('@/views/faq')
const AccountTypes = () =>
    import ('@/views/account-types')
const CookiePage = () =>
    import ('@/views/cookie')
const PrivacyPolicy = () =>
    import ('@/views/privacy-policy')

const Mt5Ipad = () =>
    import ('@/views/mt5-ipad')
const Contact = () =>
    import ('@/views/contact')
const Mt5Html = () =>
    import ('@/views/page/mt5-html')
const ProductIntroduce = () =>
    import ('@/views/product-introduce')
const ProductIntroduction = () =>
    import ('@/views/product-Introduction')



//const HomeEN = () =>
//    import ('@/views/home-en')
//// const Mt5EN = () =>
////     import ('@/views/page-en/mt5')
//    // const Mt5Mac = () => import('@/views/page-en/mt5-mac')
//// const Mt5WebEN = () =>
////     import ('@/views/page-en/mt5-webtrader')
//// const Mt5IphoneEN = () =>
////     import ('@/views/page-en/mt5-iphone')
//// const Mt5AndroidEN = () =>
////     import ('@/views/page-en/mt5-android')

//const ForexTradingEN = () =>
//    import ('@/views/page-en/forex-trading')
//const CommoditiesEN = () =>
//    import ('@/views/page-en/commodities')
//const EquityIndicesEN = () =>
//    import ('@/views/page-en/equity-indices')
//const PreciousMetalsEN = () =>
//    import ('@/views/page-en/precious-metals')
//const EnergiesEN = () =>
//    import ('@/views/page-en/Energies')
//    // const Shares = () => import('@/views/page-en/shares')
//const ExecutionPolicyEN = () =>
//    import ('@/views/page-en/execution-policy')
//const SpreadsEN = () =>
//    import ('@/views/page-en/spreads')
//const MarginAndLeverageEN = () =>
//    import ('@/views/page-en/margin-and-leverage')
//const OvernightPositionsEN = () =>
//    import ('@/views/page-en/overnight-positions')
//const TradingHoursEN = () =>
//    import ('@/views/page-en/trading-hours')
//// const AboutEN = () =>
////     import ('@/views/page-en/about')
//// const RegulationEN = () =>
////     import ('@/views/page-en/regulation')
//// const LegalDocumentsEN = () =>
////     import ('@/views/page-en/legal-documents')
//const FAQEN = () =>
//    import ('@/views/page-en/faq')
//const AccountTypesEN = () =>
//    import ('@/views/page-en/account-types')
//const CookiePageEN = () =>
//    import ('@/views/page-en/cookie')
//const PrivacyPolicyEN = () =>
//    import ('@/views/page-en/privacy-policy')

// const Mt5IpadEN = () =>
//     import ('@/views/page-en/mt5-ipad')

//// const ContactEN = () =>
////     import ('@/views/page-en/contact')
//const ProductIntroduceEN = () =>
//    import ('@/views/page-en/product-introduce')
//const AnalyticsInfoEN = () =>
//    import ('@/views/page-en/analytics-info')
//    // const Mt5HtmlEN = () => import('@/views/page-en/mt5-html')
//const ProductIntroduceTR = () =>
//    import ('@/views/page-tur/product-introduce')
//const ProductIntroduceAR = () =>
//    import ('@/views/page-ar/product-introduce')
//const ProductIntroduceIR = () =>
//    import ('@/views/page-ir/product-introduce')
//const AnalyticsInfoTR = () =>
//    import ('@/views/page-tur/analytics-info')
const AnalyticsInfo = () =>
    import ('@/views/analytics-info')



//const HomeTR = () =>
//    import ('@/views/home-tur')


////const ProductsTR = () => import('@/views/page-tur/products')
////const PublishTR = () => import('@/views/page-tur/publish')
////const FOrderTR = () => import('@/views/page-tur/f-order')
////const MyProductsTR = () => import('@/views/page-tur/my-products')
////const ArticleListTR = () => import('@/views/page-tur/article-list')
////const ArticleDetailsTR = () => import('@/views/page-tur/article-details')
////const ContractTR = () => import('@/views/page-tur/contract')
////const ContractENTR = () => import('@/views/page-tur/contract-en')
////const ActivityListTR = () => import('@/views/page-tur/activity-list')
////const AgentTR = () => import('@/views/page-tur/agent')

//// const Mt5TR = () =>
////     import ('@/views/page-tur/mt5')
//const Mt5WebTR = () =>
//    import ('@/views/page-tur/mt5-webtrader')
//// const Mt5IphoneTR = () =>
////     import ('@/views/page-tur/mt5-iphone')
//// const Mt5AndroidTR = () =>
////     import ('@/views/page-tur/mt5-android')

//const ForexTradingTR = () =>
//    import ('@/views/page-tur/forex-trading')
//const CommoditiesTR = () =>
//    import ('@/views/page-tur/commodities')
//const EquityIndicesTR = () =>
//    import ('@/views/page-tur/equity-indices')
//const PreciousMetalsTR = () =>
//    import ('@/views/page-tur/precious-metals')
//const EnergiesTR = () =>
//    import ('@/views/page-tur/Energies')
//const ExecutionPolicyTR = () =>
//    import ('@/views/page-tur/execution-policy')
//const SpreadsTR = () =>
//    import ('@/views/page-tur/spreads')
//const MarginAndLeverageTR = () =>
//    import ('@/views/page-tur/margin-and-leverage')
//const OvernightPositionsTR = () =>
//    import ('@/views/page-tur/overnight-positions')
//const TradingHoursTR = () =>
//    import ('@/views/page-tur/trading-hours')
//// const AboutTR = () =>
////     import ('@/views/page-tur/about')
//// const RegulationTR = () =>
////     import ('@/views/page-tur/regulation')
//// const LegalDocumentsTR = () =>
////     import ('@/views/page-tur/legal-documents')
//const FAQTR = () =>
//    import ('@/views/page-tur/faq')
//const AccountTypesTR = () =>
//    import ('@/views/page-tur/account-types')
//const CookiePageTR = () =>
//    import ('@/views/page-tur/cookie')
//const PrivacyPolicyTR = () =>
//    import ('@/views/page-tur/privacy-policy')

//// const Mt5IpadTR = () =>
////     import ('@/views/page-tur/mt5-ipad')
//// const ContactTR = () =>
////     import ('@/views/page-tur/contact')

////AR routes
//const HomeAR = () =>
//    import ('@/views/home-ar')
//// const Mt5AR = () =>
////     import ('@/views/page-ar/mt5')

//// const Mt5WebAR = () =>
////     import ('@/views/page-ar/mt5-webtrader')

//// const Mt5IphoneAR = () =>
////     import ('@/views/page-ar/mt5-iphone')

//// const Mt5AndroidAR = () =>
////     import ('@/views/page-ar/mt5-android')


//const ForexTradingAR = () =>
//    import ('@/views/page-ar/forex-trading')

//const CommoditiesAR = () =>
//    import ('@/views/page-ar/commodities')

//const EquityIndicesAR = () =>
//    import ('@/views/page-ar/equity-indices')

//const PreciousMetalsAR = () =>
//    import ('@/views/page-ar/precious-metals')

//const EnergiesAR = () =>
//    import ('@/views/page-ar/Energies')

//const ExecutionPolicyAR = () =>
//    import ('@/views/page-ar/execution-policy')

//const SpreadsAR = () =>
//    import ('@/views/page-ar/spreads')

//const MarginAndLeverageAR = () =>
//    import ('@/views/page-ar/margin-and-leverage')

//const OvernightPositionsAR = () =>
//    import ('@/views/page-ar/overnight-positions')

//const TradingHoursAR = () =>
//    import ('@/views/page-ar/trading-hours')

// const AboutAR = () =>
//     import ('@/views/page-ar/about')

// const RegulationAR = () =>
//     import ('@/views/page-ar/regulation')

// const LegalDocumentsAR = () =>
//     import ('@/views/page-ar/legal-documents')

//const FAQAR = () =>
//    import ('@/views/page-ar/faq')

//const AccountTypesAR = () =>
//    import ('@/views/page-ar/account-types')

//const CookiePageAR = () =>
//    import ('@/views/page-ar/cookie')

//const PrivacyPolicyAR = () =>
//    import ('@/views/page-ar/privacy-policy')


// const Mt5IpadAR = () =>
//     import ('@/views/page-ar/mt5-ipad')

// const ContactAR = () =>
//     import ('@/views/page-ar/contact')

//const AnalyticsInfoAR = () =>
//    import ('@/views/page-ar/analytics-info')

////IR routes
//const HomeIR = () =>
//    import ('@/views/home-ir')
// const Mt5IR = () =>
//     import ('@/views/page-ir/mt5')

// const Mt5WebIR = () =>
//     import ('@/views/page-ir/mt5-webtrader')

// const Mt5IphoneIR = () =>
//     import ('@/views/page-ir/mt5-iphone')

// const Mt5AndroidIR = () =>
//     import ('@/views/page-ir/mt5-android')


//const ForexTradingIR = () =>
//    import ('@/views/page-ir/forex-trading')

//const CommoditiesIR = () =>
//    import ('@/views/page-ir/commodities')

//const EquityIndicesIR = () =>
//    import ('@/views/page-ir/equity-indices')

//const PreciousMetalsIR = () =>
//    import ('@/views/page-ir/precious-metals')

//const EnergiesIR = () =>
//    import ('@/views/page-ir/energies')

//const ExecutionPolicyIR = () =>
//    import ('@/views/page-ir/execution-policy')

//const SpreadsIR = () =>
//    import ('@/views/page-ir/spreads')

//const MarginAndLeverageIR = () =>
//    import ('@/views/page-ir/margin-and-leverage')

//const OvernightPositionsIR = () =>
//    import ('@/views/page-ir/overnight-positions')

//const TradingHoursIR = () =>
//    import ('@/views/page-ir/trading-hours')

// const AboutIR = () =>
//     import ('@/views/page-ir/about')

// const RegulationIR = () =>
//     import ('@/views/page-ir/regulation')

// const LegalDocumentsIR = () =>
//     import ('@/views/page-ir/legal-documents')

//const FAQIR = () =>
//    import ('@/views/page-ir/faq')

//const AccountTypesIR = () =>
//    import ('@/views/page-ir/account-types')

//const CookiePageIR = () =>
//    import ('@/views/page-ir/cookie')

//const PrivacyPolicyIR = () =>
//    import ('@/views/page-ir/privacy-policy')


// const Mt5IpadIR = () =>
//     import ('@/views/page-ir/mt5-ipad')

// const ContactIR = () =>
//     import ('@/views/page-ir/contact')

//const AnalyticsInfoIR = () =>
//    import ('@/views/page-ir/analytics-info')



const Login = () =>
    import ('@/views/my/login')
const Register = () =>
    import ('@/views/my/register')
const ApplicationSuccess = () =>
    import ('@/views/my/application-success')
const UserAgreement = () =>
    import ('@/views/my/user-agreement')
const SendCode = () =>
    import ('@/views/my/send-code')
const My = () =>
    import ('@/views/my/home')
const EditPwd = () =>
    import ('@/views/my/change-pwd')
const Verify = () =>
    import ('@/views/my/verify')
const VerifyLv2 = () =>
    import ('@/views/my/verify2')
const VerifyLv3 = () =>
    import ('@/views/my/verify3')

const CoinsTrad = () =>
    import ('@/views/coins_trad/index')
const CoinsBuy = () =>
    import ('@/views/coins_trad/coins-buy')
const CoinsSell = () =>
    import ('@/views/coins_trad/coins-sell')
const AddBankCard = () =>
    import ('@/views/coins_trad/add-bank')
const AddAddress = () =>
    import ('@/views/coins_trad/add-address')
const OrderUploadFile = () =>
    import ('@/views/coins_trad/order-update-file')
const Take = () =>
    import ('@/views/coins_trad/take')
const WealthCoupons = () =>
    import ('@/views/coins_trad/wealth-coupons')

// const RegisterOperator = () => import('@/views/my/register-operator')
// const OperatorInfo = () => import('@/views/my/operator-info')
// const SettingSuperior = () => import('@/views/my/setting-superior')
// const OperatorInfoChange = () => import('@/views/my/operator-info-change')
// const Entrust = () => import('@/views/my/entrust')
// const EntrustInfo = () => import('@/views/my/entrust-info')



const Badbrowser = () =>
    import ('@/views/badbrowser')


Vue.use(Router)


export default new Router({

    routes: [{
            path: '/',
            name: 'home',
            component: Home
        },
        // {
        //     path: '/zh',
        //     name: 'homeEN',
        //     component: HomeEN
        // },
        // {
        //     path: '/tr',
        //     name: 'homeTR',
        //     component: HomeTR
        // },
        // {
        //     path: '/ar',
        //     name: 'homeAR',
        //     component: HomeAR
        // },
        // {
        //     path: '/ir',
        //     name: 'homeIR',
        //     component: HomeIR
        // },
        {
            path: '/product-introduce',
            name: 'products',
            component: Products
        },
        {
            path: '/publish',
            name: 'publish',
            component: Publish
        },
        {
            path: '/f-order',
            name: 'fOrder',
            component: FOrder
        },
        {
            path: '/my-products',
            name: 'myProducts',
            component: MyProducts
        },
        {
            path: '/article-list',
            name: 'articleList',
            component: ArticleList
        },
        {
            path: '/activity-list',
            name: 'activityList',
            component: ActivityList
        },

        {
            path: '/article-details',
            name: 'articleDetails',
            component: ArticleDetails
        },
        {
            path: '/agent',
            name: 'agent',
            component: Agent
        },
        {
            path: '/mt5',
            name: 'mt5',
            component: MT5
        },
        {
            path: '/contract',
            name: 'contract',
            component: Contract
        },
        //{
        //    path: '/contract-en',
        //    name: 'contractEN',
        //    component: ContractEN
        //},
        {
            path: '/mt5-webtrader',
            name: 'mt5Web',
            component: Mt5Web
        },
        {
            path: '/mt5-iphone',
            name: 'mt5Iphone',
            component: Mt5Iphone
        },
        {
            path: '/mt5-android',
            name: 'mt5Android',
            component: Mt5Android
        },
        {
            path: '/forex-trading',
            name: 'forexTrading',
            component: ForexTrading
        },
        {
            path: '/commodities',
            name: 'commodities',
            component: Commodities
        },
        {
            path: '/equity-indices',
            name: 'equityIndices',
            component: EquityIndices
        },
        {
            path: '/precious-metals',
            name: 'preciousMetals',
            component: PreciousMetals
        },
        {
            path: '/execution-policy',
            name: 'executionPolicy',
            component: ExecutionPolicy
        },
        {
            path: '/energies',
            name: 'energies',
            component: Energies
        },

        {
            path: '/spreads',
            name: 'spreads',
            component: Spreads
        },
        {
            path: '/margin-and-Leverage',
            name: 'marginAndLeverage',
            component: MarginAndLeverage
        },
        {
            path: '/overnight-positions',
            name: 'overnightPositions',
            component: OvernightPositions
        },
        {
            path: '/trading-hours',
            name: 'tradingHours',
            component: TradingHours
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/legal-documents',
            name: 'legalDocuments',
            component: LegalDocuments
        },



        {
            path: '/regulation',
            name: 'regulation',
            component: Regulation
        },
        {
            path: '/faq',
            name: 'faq',
            component: FAQ
        },
        {
            path: '/account-types',
            name: 'accountTypes',
            component: AccountTypes
        },
        {
            path: '/cookie',
            name: 'cookiePage',
            component: CookiePage
        },
        {
            path: '/privacy-policy',
            name: 'privacyPolicy',
            component: PrivacyPolicy
        },

        {
            path: '/mt5-ipad',
            name: 'mt5Ipad',
            component: Mt5Ipad
        },

        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },

        {
            path: '/mt5-html',
            name: 'mt5Html',
            component: Mt5Html
        },
        {
            path: '/product-introduce',
            name: 'productIntroduce',
            component: ProductIntroduce
        },
        {
            path: '/product-Introduction',
            name: 'productIntroduction',
            component: ProductIntroduction
        },
        {
            path: '/analytics-info',
            name: 'analyticsInfo',
            component: AnalyticsInfo
        },
        /////////////////////////////////////////////////////////


        // {
        //     path: '/en/mt5',
        //     name: 'mt5EN',
        //     component: Mt5EN
        // },

        // {
        //     path: '/en/mt5-webtrader',
        //     name: 'mt5WebEN',
        //     component: Mt5WebEN
        // },
        // {
        //     path: '/en/mt5-iphone',
        //     name: 'mt5IphoneEN',
        //     component: Mt5IphoneEN
        // },
        // {
        //     path: '/en/mt5-android',
        //     name: 'mt5AndroidEN',
        //     component: Mt5AndroidEN
        // },
        // {
        //     path: '/en/forex-trading',
        //     name: 'forexTradingEN',
        //     component: ForexTradingEN
        // },
        //{
        //    path: '/en/commodities',
        //    name: 'commoditiesEN',
        //    component: CommoditiesEN
        //},
        //{
        //    path: '/en/equity-indices',
        //    name: 'equityIndicesEN',
        //    component: EquityIndicesEN
        //},
        // {
        //     path: '/en/precious-metals',
        //     name: 'preciousMetalsEN',
        //     component: PreciousMetalsEN
        // },
        // {
        //     path: '/en/execution-policy',
        //     name: 'executionPolicyEN',
        //     component: ExecutionPolicyEN
        // },
        // {
        //     path: '/en/energies',
        //     name: 'energiesEN',
        //     component: EnergiesEN
        // },

        // {
        //     path: '/en/spreads',
        //     name: 'spreadsEN',
        //     component: SpreadsEN
        // },
        // {
        //     path: '/en/margin-and-Leverage',
        //     name: 'marginAndLeverageEN',
        //     component: MarginAndLeverageEN
        // },
        // {
        //     path: '/en/overnight-positions',
        //     name: 'overnightPositionsEN',
        //     component: OvernightPositionsEN
        // },
        // {
        //     path: '/en/trading-hours',
        //     name: 'tradingHoursEN',
        //     component: TradingHoursEN
        // },
        // {
        //     path: '/en/about',
        //     name: 'aboutEN',
        //     component: AboutEN
        // },
        // {
        //     path: '/en/legal-documents',
        //     name: 'legalDocumentsEN',
        //     component: LegalDocumentsEN
        // },



        // {
        //     path: '/en/regulation',
        //     name: 'regulationEN',
        //     component: RegulationEN
        // },

        // {
        //     path: '/en/faq',
        //     name: 'faqEN',
        //     component: FAQEN
        // },
        // {
        //     path: '/en/account-types',
        //     name: 'accountTypesEN',
        //     component: AccountTypesEN
        // },
        // {
        //     path: '/en/cookie',
        //     name: 'cookiePageEN',
        //     component: CookiePageEN
        // },
        // {
        //     path: '/en/privacy-policy',
        //     name: 'privacyPolicyEN',
        //     component: PrivacyPolicyEN
        // },

        // {
        //     path: '/en/mt5-ipad',
        //     name: 'mt5IpadEN',
        //     component: Mt5IpadEN
        // },

        // {
        //     path: '/en/contact',
        //     name: 'contactEN',
        //     component: ContactEN
        // },
        //{
        //    path: '/en/fm-introduce',
        //    name: 'productIntroduceEN',
        //    component: ProductIntroduceEN
        //},
        // {
        //     path: '/en/analytics-info',
        //     name: 'analyticsInfoEN',
        //     component: AnalyticsInfoEN
        // },

        ////////////////////


        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/application-success',
            name: 'applicationSuccess',
            component: ApplicationSuccess
        },

        {
            path: '/my',
            name: 'my',
            component: My
        },
        {
            path: '/my/verify',
            name: 'verify',
            component: Verify
        },
        {
            path: '/my/verify2',
            name: 'verifyLv2',
            component: VerifyLv2
        },
        {
            path: '/my/verify3',
            name: 'verifyLv3',
            component: VerifyLv3
        },
        {
            path: '/my/sendCode',
            name: 'sendCode',
            component: SendCode
        },

        {
            path: '/wealth-coupons',
            name: 'wealthCoupons',
            component: WealthCoupons
        },
        {
            path: '/coinstrad',
            name: 'coinstrad',
            component: CoinsTrad
        },

        {
            path: '/coins-buy',
            name: 'coinsBuy',
            component: CoinsBuy
        },
        {
            path: '/coins-sell',
            name: 'coinsSell',
            component: CoinsSell
        },
        {
            path: '/add-bankcard',
            name: 'addBankCard',
            component: AddBankCard
        },
        {
            path: '/order-upload-file',
            name: 'orderUploadFile',
            component: OrderUploadFile
        },
        {
            path: '/my/edit-pwd',
            name: 'editPwd',
            component: EditPwd
        },
        {
            path: '/user-agreement',
            name: 'usserAgreement',
            component: UserAgreement
        },
        {
            path: '/add-address',
            name: 'addAddress',
            component: AddAddress
        },
        {
            path: '/take',
            name: 'take',
            component: Take
        },
        {
            path: '/badbrowser',
            name: 'badbrowser',
            component: Badbrowser
        },
        //{
        //  path: '/tr/products',
        //  name: 'products',
        //  component: ProductsTR
        //},
        //{
        //  path: '/tr/publish',
        //  name: 'publish',
        //  component: PublishTR
        //},
        //{
        //  path: '/tr/f-order',
        //  name: 'fOrder',
        //  component: FOrderTR
        //},
        //{
        //  path: '/tr/my-products',
        //  name: 'myProducts',
        //  component: MyProductsTR
        //},
        //{
        //  path: '/tr/article-list',
        //  name: 'articleList',
        //  component: ArticleListTR
        //},
        //{
        //  path: '/tr/activity-list',
        //  name: 'activityList',
        //  component: ActivityListTR
        //},

        //{
        //  path: '/tr/article-details',
        //  name: 'articleDetails',
        //  component: ArticleDetailsTR
        //},
        //{
        //  path: '/tr/agent',
        //  name: 'agent',
        //  component: AgentTR
        //},
        //{
        //  path: '/tr/contract',
        //  name: 'contract',
        //  component: ContractTR
        //},
        //{
        //  path: '/tr/contract-en',
        //  name: 'contractEN',
        //  component: ContractENTR
        //},

        // {
        //     path: '/tr/mt5',
        //     name: 'mt5TR',
        //     component: Mt5TR
        // },

        // {
        //     path: '/tr/mt5-webtrader',
        //     name: 'mt5WebTR',
        //     component: Mt5WebTR
        // },
        // {
        //     path: '/tr/mt5-iphone',
        //     name: 'mt5IphoneTR',
        //     component: Mt5IphoneTR
        // },
        // {
        //     path: '/tr/mt5-android',
        //     name: 'mt5AndroidTR',
        //     component: Mt5AndroidTR
        // },
        // {
        //     path: '/tr/forex-trading',
        //     name: 'forexTradingTR',
        //     component: ForexTradingTR
        // },
        //{
        //    path: '/tr/commodities',
        //    name: 'commoditiesTR',
        //    component: CommoditiesTR
        //},
        //{
        //    path: '/tr/equity-indices',
        //    name: 'equityIndicesTR',
        //    component: EquityIndicesTR
        //},
        // {
        //     path: '/tr/precious-metals',
        //     name: 'preciousMetalsTR',
        //     component: PreciousMetalsTR
        // },
        // {
        //     path: '/tr/execution-policy',
        //     name: 'executionPolicyTR',
        //     component: ExecutionPolicyTR
        // },
        // {
        //     path: '/tr/energies',
        //     name: 'energiesTR',
        //     component: EnergiesTR
        // },

        // {
        //     path: '/tr/spreads',
        //     name: 'spreadsTR',
        //     component: SpreadsTR
        // },
        // {
        //     path: '/tr/margin-and-Leverage',
        //     name: 'marginAndLeverageTR',
        //     component: MarginAndLeverageTR
        // },
        // {
        //     path: '/tr/overnight-positions',
        //     name: 'overnightPositionsTR',
        //     component: OvernightPositionsTR
        // },
        // {
        //     path: '/tr/trading-hours',
        //     name: 'tradingHoursTR',
        //     component: TradingHoursTR
        // },
        // {
        //     path: '/tr/about',
        //     name: 'aboutTR',
        //     component: AboutTR
        // },
        // {
        //     path: '/tr/legal-documents',
        //     name: 'legalDocumentsTR',
        //     component: LegalDocumentsTR
        // },



        // {
        //     path: '/tr/regulation',
        //     name: 'regulationTR',
        //     component: RegulationTR
        // },
        // {
        //     path: '/tr/faq',
        //     name: 'faqTR',
        //     component: FAQTR
        // },
        // {
        //     path: '/tr/account-types',
        //     name: 'accountTypesTR',
        //     component: AccountTypesTR
        // },
        // {
        //     path: '/tr/cookie',
        //     name: 'cookiePageTR',
        //     component: CookiePageTR
        // },
        // {
        //     path: '/tr/privacy-policy',
        //     name: 'privacyPolicyTR',
        //     component: PrivacyPolicyTR
        // },

        // {
        //     path: '/tr/mt5-ipad',
        //     name: 'mt5IpadTR',
        //     component: Mt5IpadTR
        // },

        // {
        //     path: '/tr/contact',
        //     name: 'contactTR',
        //     component: ContactTR
        // },
        //{
        //    path: '/tr/fm-introduce',
        //    name: 'productIntroduceTR',
        //    component: ProductIntroduceTR
        //},
        // {
        //     path: '/tr/analytics-info',
        //     name: 'analyticsInfoTR',
        //     component: AnalyticsInfoTR
        // },

        //AR routes

        // {
        //     path: '/ar/mt5',
        //     name: 'mt5AR',
        //     component: Mt5AR
        // },

        // {
        //     path: '/ar/mt5-webtrader',
        //     name: 'mt5WebAR',
        //     component: Mt5WebAR
        // },
        // {
        //     path: '/ar/mt5-iphone',
        //     name: 'mt5IphoneAR',
        //     component: Mt5IphoneAR
        // },
        // {
        //     path: '/ar/mt5-android',
        //     name: 'mt5AndroidAR',
        //     component: Mt5AndroidAR
        // },
        // {
        //     path: '/ar/forex-trading',
        //     name: 'forexTradingAR',
        //     component: ForexTradingAR
        // },
        //{
        //    path: '/ar/commodities',
        //    name: 'commoditiesAR',
        //    component: CommoditiesAR
        //},
        //{
        //    path: '/ar/equity-indices',
        //    name: 'equityIndicesAR',
        //    component: EquityIndicesAR
        //},
        // {
        //     path: '/ar/precious-metals',
        //     name: 'preciousMetalsAR',
        //     component: PreciousMetalsAR
        // },
        // {
        //     path: '/ar/execution-policy',
        //     name: 'executionPolicyAR',
        //     component: ExecutionPolicyAR
        // },
        // {
        //     path: '/ar/energies',
        //     name: 'energiesAR',
        //     component: EnergiesAR
        // },

        // {
        //     path: '/ar/spreads',
        //     name: 'spreadsAR',
        //     component: SpreadsAR
        // },
        // {
        //     path: '/ar/margin-and-Leverage',
        //     name: 'marginAndLeverageAR',
        //     component: MarginAndLeverageAR
        // },
        // {
        //     path: '/ar/overnight-positions',
        //     name: 'overnightPositionsAR',
        //     component: OvernightPositionsAR
        // },
        // {
        //     path: '/ar/trading-hours',
        //     name: 'tradingHoursAR',
        //     component: TradingHoursAR
        // },
        // {
        //     path: '/ar/about',
        //     name: 'aboutAR',
        //     component: AboutAR
        // },
        // {
        //     path: '/ar/legal-documents',
        //     name: 'legalDocumentsAR',
        //     component: LegalDocumentsAR
        // },



        // {
        //     path: '/ar/regulation',
        //     name: 'regulationAR',
        //     component: RegulationAR
        // },

        // {
        //     path: '/ar/faq',
        //     name: 'faqAR',
        //     component: FAQAR
        // },
        // {
        //     path: '/ar/account-types',
        //     name: 'accountTypesAR',
        //     component: AccountTypesAR
        // },
        // {
        //     path: '/ar/cookie',
        //     name: 'cookiePageAR',
        //     component: CookiePageAR
        // },
        // {
        //     path: '/ar/privacy-policy',
        //     name: 'privacyPolicyAR',
        //     component: PrivacyPolicyAR
        // },

        // {
        //     path: '/ar/mt5-ipad',
        //     name: 'mt5IpadAR',
        //     component: Mt5IpadAR
        // },

        // {
        //     path: '/ar/contact',
        //     name: 'contactAR',
        //     component: ContactAR
        // },
        //{
        //    path: '/ar/fm-introduce',
        //    name: 'productIntroduceAR',
        //    component: ProductIntroduceAR
        //},
        // {
        //     path: '/ar/analytics-info',
        //     name: 'analyticsInfoAR',
        //     component: AnalyticsInfoAR
        // },
        //IR routes

        // {
        //     path: '/ir/mt5',
        //     name: 'mt5IR',
        //     component: Mt5IR
        // },

        // {
        //     path: '/ir/mt5-webtrader',
        //     name: 'mt5WebIR',
        //     component: Mt5WebIR
        // },
        // {
        //     path: '/ir/mt5-iphone',
        //     name: 'mt5IphoneIR',
        //     component: Mt5IphoneIR
        // },
        // {
        //     path: '/ir/mt5-android',
        //     name: 'mt5AndroidIR',
        //     component: Mt5AndroidIR
        // },
        // {
        //     path: '/ir/forex-trading',
        //     name: 'forexTradingIR',
        //     component: ForexTradingIR
        // },
        // {
        //     path: '/ir/precious-metals',
        //     name: 'preciousMetalsIR',
        //     component: PreciousMetalsIR
        // },
        // {
        //     path: '/ir/execution-policy',
        //     name: 'executionPolicyIR',
        //     component: ExecutionPolicyIR
        // },
        // {
        //     path: '/ir/energies',
        //     name: 'energiesIR',
        //     component: EnergiesIR
        // },

        // {
        //     path: '/ir/spreads',
        //     name: 'spreadsIR',
        //     component: SpreadsIR
        // },
        // {
        //     path: '/ir/margin-and-Leverage',
        //     name: 'marginAndLeverageIR',
        //     component: MarginAndLeverageIR
        // },
        // {
        //     path: '/ir/overnight-positions',
        //     name: 'overnightPositionsIR',
        //     component: OvernightPositionsIR
        // },
        // {
        //     path: '/ir/trading-hours',
        //     name: 'tradingHoursIR',
        //     component: TradingHoursIR
        // },
        // {
        //     path: '/ir/about',
        //     name: 'aboutIR',
        //     component: AboutIR
        // },
        // {
        //     path: '/ir/legal-documents',
        //     name: 'legalDocumentsIR',
        //     component: LegalDocumentsIR
        // },



        // {
        //     path: '/ir/regulation',
        //     name: 'regulationIR',
        //     component: RegulationIR
        // },

        // {
        //     path: '/ir/faq',

        //     name: 'faqIR',
        //     component: FAQIR
        // },
        // {
        //     path: '/ir/account-types',
        //     name: 'accountTypesIR',
        //     component: AccountTypesIR
        // },
        // {
        //     path: '/ir/cookie',
        //     name: 'cookiePageIR',
        //     component: CookiePageIR
        // },
        // {
        //     path: '/ir/privacy-policy',
        //     name: 'privacyPolicyIR',
        //     component: PrivacyPolicyIR
        // },

        // {
        //     path: '/ir/mt5-ipad',
        //     name: 'mt5IpadIR',
        //     component: Mt5IpadIR
        // },

        // {
        //     path: '/ir/contact',
        //     name: 'contactIR',
        //     component: ContactIR
        // },
        //{
        //    path: '/ir/fm-introduce',
        //    name: 'productIntroduceIR',
        //    component: ProductIntroduceIR
        //},
        // {
        //     path: '/ir/analytics-info',
        //     name: 'analyticsInfoIR',
        //     component: AnalyticsInfoIR
        // },


        //{
        //  path: '/tr/mt5-html',
        //  name: 'mt5Html',
        //  component: Mt5HtmlTR
        //},
        //{
        //  path: '/tr/fm-introduce',
        //  name: 'productIntroduce',
        //  component: ProductIntroduceTR
        //},
        /////////////////////////////////////////////////////////


    ]
})