(function() {
  (function($) {
    $.fn.sortTable = function(sortFns) {
      return this.each(function() {
        var $table, apply_sort_map, sort_map;
        $table = $(this);
        sortFns = sortFns || {};
        sortFns = $.extend({}, {
          int: function(a, b) {
            return parseInt(a, 10) - parseInt(b, 10);
          },
          float: function(a, b) {
            return parseFloat(a) - parseFloat(b);
          },
          string: function(a, b) {
            if (a < b) {
              return -1;
            }
            if (a > b) {
              return +1;
            }
            return 0;
          },
          "string-ins": function(a, b) {
            a = a.toLowerCase();
            b = b.toLowerCase();
            if (a < b) {
              return -1;
            }
            if (a > b) {
              return +1;
            }
            return 0;
          }
        }, sortFns);
        sort_map = function(arr, sort_function, reverse_column) {
          var i, index, map, sorted;
          map = [];
          index = 0;
          if (reverse_column) {
            i = arr.length - 1;
            while (i >= 0) {
              map.push(i);
              i--;
            }
          } else {
            sorted = arr.slice(0).sort(sort_function);
            i = 0;
            while (i < arr.length) {
              index = $.inArray(arr[i], sorted);
              while ($.inArray(index, map) !== -1) {
                index++;
              }
              map.push(index);
              i++;
            }
          }
          return map;
        };
        apply_sort_map = function(arr, map) {
          var clone, i, newIndex;
          clone = arr.slice(0);
          newIndex = 0;
          i = 0;
          while (i < map.length) {
            newIndex = map[i];
            clone[newIndex] = arr[i];
            i++;
          }
          return clone;
        };
        return $table.on("click", "th", function() {
          var $this, dir, sort_dir, th_index, trs, type;
          trs = $table.children("tbody").children("tr");
          $this = $(this);
          th_index = 0;
          dir = $.fn.sortTable.dir;
          $table.find("th").slice(0, $this.index()).each(function() {
            var cols;
            cols = $(this).attr("colspan") || 1;
            return th_index += parseInt(cols, 10);
          });
          sort_dir = ($this.data("sort-dir") === dir.ASC ? dir.DESC : dir.ASC);
          if (sort_dir === dir.DESC) {
            type = $this.data("sort-desc") || $this.data("sort") || null;
          } else {
            type = $this.data("sort") || null;
          }
          if (type === null) {
            return;
          }
          $table.trigger("beforetablesort", {
            column: th_index,
            direction: sort_dir
          });
          $table.css("display");
          return setTimeout((function() {
            var column, reverse_column, sortMethod, sortedTRs, theMap;
            column = [];
            sortMethod = sortFns[type];
            trs.each(function(index, tr) {
              var $e, order_by, sort_val;
              $e = $(tr).children().eq(th_index);
              sort_val = $e.data("sort-value");
              order_by = (typeof sort_val !== "undefined" ? sort_val : $e.text());
              return column.push(order_by);
            });
            reverse_column = !!$this.data("sort-dir") && !$this.data("sort-desc");
            theMap = sort_map(column, sortMethod, reverse_column);
            $table.find("th").data("sort-dir", null).removeClass("sorting-desc sorting-asc");
            $this.data("sort-dir", sort_dir).addClass("sorting-" + sort_dir);
            sortedTRs = $(apply_sort_map(trs, theMap));
            $table.children("tbody").append(sortedTRs);
            $table.trigger("aftertablesort", {
              column: th_index,
              direction: sort_dir
            });
            return $table.css("display");
          }), 10);
        });
      });
    };
    $.fn.sortTable.dir = {
      ASC: "asc",
      DESC: "desc"
    };
    return $("table.table-team").sortTable();
  })(jQuery);

}).call(this);